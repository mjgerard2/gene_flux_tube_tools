import os
from netCDF4 import Dataset

import h5py as hf
import numpy as np
import read_parameters as rp

import scipy.interpolate as spi
import scipy.constants as const


class readBooz:
    """
    A class to read an ./xbooz_xform netCDF output

    ...

    Attributes
    ----------
    path : str
        path to the directory in which the xbooz file is located
    name : str, optional
        name of the wout file being read.  Default is 'wout_HSX_main_opt0.nc'
    space_derivs : bool, optional
        read differential flux coordinates and include in fourier amplitudes.
        Default is False
    field_derivs : bool, optional
        read B field differentials necessary to perform B field curvature
        calculations and include in fourier amplitudes. Default is False

    Methods
    -------
    transForm_3D(u_dom, v_dom, ampKeys, errKeys=False)
        Performs 3D Fourier transform on specified keys

    transForm_2D_sSec(s_val, u_dom, v_dom, ampKeys, errKeys=False)
        Performs 2D Fourier transform along one flux surface on specified keys

    boozCoord_2D_sSec(s_val, u_dom, v_dom, ampKeys)
        Performs 2D Fourier transform along one flux surface on specified keys

    transForm_1D(s_val, u, v, ampKeys, errKeys=False)
        Performs 1D Fourier transform at a particular flux coordinate on
        specified keys

    transForm_listPoints(points, ampKeys, errKeys=False):
        Performs 1D Fourier transform at listed flux coordinates on
        specified keys

    calc_metric_tensor(s_dom, u_dom, v_dom)
        Calculate the metric tensor components.

    follow_field_line(s, alpha, nz, n_pol, ampKeys)
        Follow a magnetic field line following the GIST specification parameters.

    flux_tube_geometry(s0, a0, pram_file, ampKeys, Ninterp=0)
        Transform GENE flux-tube coordinates into Cartesian coordinates.

    plot_spectrum(self, plot, nSpec=10, excFirst=True, savePath=None):
        Plots the most prominant modes according to the Fourier amplitude
        specified.  The first mode is excluded by default.

    Raises
    ------
    IOError
        Boozer and/or VMEC files do not exist.
    """

    def __init__(self, booz_path, vmec_path, space_derivs=False, field_derivs=False, trans_derivs=False):
        try:
            booz_file = Dataset(booz_path, 'r')
        except IOError:
            raise IOError('Boozer file does not exists: ' + booz_path)

        try:
            vmec_file = Dataset(vmec_path, 'r')
        except IOError:
            raise IOError('VMEC file does not exists: ' + vmec_path)

        self.booz_path = booz_path
        self.vmec_path = vmec_path

        self.space_derivs = space_derivs
        self.field_derivs = field_derivs

        # Read in VMEC Data #
        self.ns_vmec = vmec_file['/ns'][0]
        self.s_vmec = np.linspace(0, 1, self.ns_vmec)

        self.xm_vmec = vmec_file['/xm'][:]
        self.xn_vmec = vmec_file['/xn'][:]
        self.md_vmec = self.xm_vmec.shape[0]

        self.iota = spi.interp1d(self.s_vmec, vmec_file['/iotaf'][:])
        self.lambda_amps = spi.interp1d(self.s_vmec, vmec_file['/lmns'][:, :], axis=0)

        # Read in Boozer Data #
        ns = booz_file['/ns_b'][0] - 1
        nsurf = booz_file['/rmnc_b'][()].shape[0]

        self.ns_booz = booz_file['/ns_b'][0] - (1 + ns - nsurf)
        self.ds = self.s_vmec[1] - self.s_vmec[0]

        s_booz_beg = .5 * self.ds
        s_booz_end = 1 - (0.5 + ns - nsurf) * self.ds
        self.s_booz = np.linspace(s_booz_beg, s_booz_end, self.ns_booz)

        self.xm_booz = booz_file['/ixm_b'][:]
        self.xn_booz = booz_file['/ixn_b'][:]
        self.md_booz = self.xm_booz.shape[0]

        self.fourierAmps = {'R': spi.interp1d(self.s_booz, booz_file['/rmnc_b'][:, :], axis=0),
                            'Z': spi.interp1d(self.s_booz, booz_file['/zmns_b'][:, :], axis=0),
                            'P': spi.interp1d(self.s_booz, booz_file['/pmns_b'][:, :], axis=0),
                            'Jacobian': spi.interp1d(self.s_booz, booz_file['/gmn_b'][:, :], axis=0),
                            'Bmod': spi.interp1d(self.s_booz, booz_file['/bmnc_b'][:, :], axis=0)}

        self.cosine_keys = ['R', 'Jacobian', 'Bmod']
        self.sine_keys = ['Z', 'P']

        if space_derivs:
            self.fourierAmps['dR_ds'] = spi.interp1d(self.s_booz, np.gradient(booz_file['/rmnc_b'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dR_du'] = spi.interp1d(self.s_booz, -booz_file['/rmnc_b'][:, :]*self.xm_booz, axis=0)
            self.fourierAmps['dR_dv'] = spi.interp1d(self.s_booz, booz_file['/rmnc_b'][:, :]*self.xn_booz, axis=0)

            self.fourierAmps['dZ_ds'] = spi.interp1d(self.s_booz, np.gradient(booz_file['/zmns_b'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dZ_du'] = spi.interp1d(self.s_booz, booz_file['/zmns_b'][:, :]*self.xm_booz, axis=0)
            self.fourierAmps['dZ_dv'] = spi.interp1d(self.s_booz, -booz_file['/zmns_b'][:, :]*self.xn_booz, axis=0)

            self.cosine_keys.extend(['dR_ds', 'dZ_du', 'dZ_dv'])
            self.sine_keys.extend(['dR_du', 'dR_dv', 'dZ_ds'])

        if field_derivs:
            self.fourierAmps['dBmod_ds'] = spi.interp1d(self.s_booz, np.gradient(booz_file['/bmnc_b'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dBmod_du'] = spi.interp1d(self.s_booz, -booz_file['/bmnc_b'][:, :] * self.xm_booz, axis=0)
            self.fourierAmps['dBmod_dv'] = spi.interp1d(self.s_booz, booz_file['/bmnc_b'][:, :] * self.xn_booz, axis=0)

            self.cosine_keys.extend(['dBmod_ds'])
            self.sine_keys.extend(['dBmod_du', 'dBmod_dv'])

        if trans_derivs:
            self.dP_du = spi.interp1d(self.s_booz, -booz_file['/pmns_b'][:, :]*self.xm_booz, axis=0)
            self.dP_dv = spi.interp1d(self.s_booz, booz_file['/pmns_b'][:, :]*self.xn_booz, axis=0)

            self.dLambda_du = spi.interp1d(self.s_vmec, -vmec_file['/lmns'][:, :]*self.xm_vmec, axis=0)
            self.dLambda_dv = spi.interp1d(self.s_vmec, vmec_file['/lmns'][:, :]*self.xn_vmec, axis=0)

        vmec_file.close()
        booz_file.close()

    def transForm_3D(self, s_dom, u_dom, v_dom, ampKeys):
        """ Performs 3D Fourier transform on specified keys

        Parameters
        ----------
        s_dom : array
            radial domain on which to perform Fourier transform
        u_dom : array
            poloidal domain on which to perform Fourier transform
        v_dom : array
            toroidal domain on which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        # Construct VMEC Coodinate Flux Surface Mesh #
        s_num = s_dom.shape[0]
        u_num = u_dom.shape[0]
        v_num = v_dom.shape[0]

        uv_num = u_num * v_num

        pol, tor = np.meshgrid(u_dom, v_dom)

        pol_xm_vmec = np.dot(self.xm_vmec.reshape(self.md_vmec, 1), pol.reshape(1, v_num * u_num))
        tor_xn_vmec = np.dot(self.xn_vmec.reshape(self.md_vmec, 1), tor.reshape(1, v_num * u_num))

        pol_xm_booz = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol.reshape(1, v_num * u_num))
        tor_xn_booz = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor.reshape(1, v_num * u_num))

        # Calculate Inverse VMEC Fourier Transforms #
        sin_mu_nv_vmec = np.sin(pol_xm_vmec - tor_xn_vmec)
        sin_mu_nv_booz = np.sin(pol_xm_booz - tor_xn_booz)

        Lambda = np.dot(self.lambda_amps(s_dom), sin_mu_nv_vmec).reshape(s_num, v_num, u_num)
        P = np.dot(self.fourierAmps['P'](s_dom), sin_mu_nv_booz).reshape(s_num, v_num, u_num)

        # Transform to Boozer Coordinates #
        pol = np.repeat(pol.reshape(1, v_num, u_num), s_num, axis=0)
        tor = np.repeat(tor.reshape(1, v_num, u_num), s_num, axis=0)

        iota_grid = np.tile(self.iota(s_dom), v_num).reshape(v_num, s_num).T
        iota_grid = np.repeat(iota_grid, u_num).reshape(s_num, v_num, u_num)

        pol = pol + Lambda - iota_grid * P
        tor = tor - P

        pol_xm = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol.reshape(1, s_num * v_num * u_num)).reshape(self.md_booz, s_num, uv_num)
        tor_xn = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor.reshape(1, s_num * v_num * u_num)).reshape(self.md_booz, s_num, uv_num)

        pol_xm = np.swapaxes(pol_xm, 0, 1)
        tor_xn = np.swapaxes(tor_xn, 0, 1)

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        # Calculate Inverse Boozer Fourier Transforms #
        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_dom)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.empty((s_num, v_num, u_num))
                for idx, s_val in enumerate(s_dom):
                    self.invFourAmps[key][idx] = np.dot(fourAmps[idx], cos_mu_nv[idx]).reshape(1, v_num, u_num)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.empty((self.ns_booz, v_num, u_num))
                for idx, s_val in enumerate(s_dom):
                    self.invFourAmps[key][idx] = np.dot(fourAmps[idx], sin_mu_nv[idx]).reshape(1, v_num, u_num)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_2D_sSec(self, s_val, u_dom, v_dom, ampKeys):
        """ Performs 2D Fourier transform along one flux surface on specified keys

        Parameters
        ----------
        s_val : float
            effective radius of flux surface on which Fourier tronsfrom will
            be performed
        u_dom : array
            poloidal domain on which to perform Fourier transform
        v_dom : array
            toroidal domain on which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        # Construct VMEC Coordinate Flux Surface Mesh #
        self.s_num = 1
        self.u_num = u_dom.shape[0]
        self.v_num = v_dom.shape[0]

        self.u_dom = u_dom
        self.v_dom = v_dom

        pol, tor = np.meshgrid(u_dom, v_dom)

        pol_xm_vmec = np.dot(self.xm_vmec.reshape(self.md_vmec, 1), pol.reshape(1, self.v_num * self.u_num))
        tor_xn_vmec = np.dot(self.xn_vmec.reshape(self.md_vmec, 1), tor.reshape(1, self.v_num * self.u_num))

        pol_xm_booz = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol.reshape(1, self.v_num * self.u_num))
        tor_xn_booz = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor.reshape(1, self.v_num * self.u_num))

        # Calculate Inverse VMEC Fourier Transforms #
        sin_mu_nv_vmec = np.sin(pol_xm_vmec - tor_xn_vmec)
        sin_mu_nv_booz = np.sin(pol_xm_booz - tor_xn_booz)

        Lambda = np.dot(self.lambda_amps(s_val), sin_mu_nv_vmec).reshape(1, self.v_num, self.u_num)
        P = np.dot(self.fourierAmps['P'](s_val), sin_mu_nv_booz).reshape(1, self.v_num, self.u_num)

        # Transform to Boozer Coordinates #
        pol = pol + Lambda - self.iota(s_val) * P
        tor = tor - P

        pol_xm = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol.reshape(1, self.v_num * self.u_num))
        tor_xn = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor.reshape(1, self.v_num * self.u_num))

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        self.pol_dom = pol
        self.tor_dom = tor

        # Calculate Inverse Boozer Fourier Transforms #
        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_val)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv).reshape(self.v_num, self.u_num)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv).reshape(self.v_num, self.u_num)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def boozCoord_2D_sSec(self, s_val, u_dom, v_dom, ampKeys):
        """ Performs 2D Fourier transform along one flux surface on specified keys

        Parameters
        ----------
        s_val : float
            effective radius of flux surface on which Fourier tronsfrom will
            be performed
        u_dom : array
            poloidal domain on which to perform Fourier transform
        v_dom : array
            toroidal domain on which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        # Construct VMEC Coordinate Flux Surface Mesh #
        self.s_num = 1
        self.u_num = u_dom.shape[0]
        self.v_num = v_dom.shape[0]

        pol, tor = np.meshgrid(u_dom, v_dom)
        pol_xm = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol.reshape(1, self.v_num * self.u_num))
        tor_xn = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor.reshape(1, self.v_num * self.u_num))

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        self.pol_dom = pol
        self.tor_dom = tor

        # Calculate Inverse Boozer Fourier Transforms #
        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_val)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv).reshape(self.v_num, self.u_num)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv).reshape(self.v_num, self.u_num)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_1D(self, s_val, u_val, v_val, ampKeys):
        """ Performs 1D Fourier transform at specified flux coordinates on
        specified keys

        Parameters
        ----------
        s_val : float
            effective radius at which to perform Fourier tronsfrom
        u_val : float
            poloidal coordinate at which to perform Fourier transform
        v_val : float
            toroidal coordinate at which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        # Construct VMEC Coordinate Mode Mesh #
        pol_xm_vmec = self.xm_vmec.reshape(self.md_vmec, 1) * u_val
        tor_xn_vmec = self.xn_vmec.reshape(self.md_vmec, 1) * v_val

        pol_xm_booz = self.xm_booz.reshape(self.md_booz, 1) * u_val
        tor_xn_booz = self.xn_booz.reshape(self.md_booz, 1) * v_val

        # Calculate Inverse VMEC Fourier Transforms #
        sin_mu_nv_vmec = np.sin(pol_xm_vmec - tor_xn_vmec)
        sin_mu_nv_booz = np.sin(pol_xm_booz - tor_xn_booz)

        Lambda = np.dot(self.lambda_amps(s_val), sin_mu_nv_vmec)[0]
        P = np.dot(self.fourierAmps['P'](s_val), sin_mu_nv_booz)[0]

        # Transform to Boozer Coordinates #
        u_val = u_val + Lambda - self.iota(s_val) * P
        v_val = v_val - P

        pol_xm = self.xm_booz.reshape(self.md_booz, 1) * u_val
        tor_xn = self.xn_booz.reshape(self.md_booz, 1) * v_val

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        # Calculate Inverse Boozer Fourier Transforms #
        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_val)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv)[0]

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv)[0]

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_listPoints(self, points, ampKeys):
        """ Performs 1D Fourier transform at specified flux coordinates on
        specified keys

        Parameters
        ----------
        points : array
            List of flux coordinates to perform 1D transform on, ordered as
            {s,u,v}
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        self.invFourAmps = {}
        for key in ampKeys:
            self.invFourAmps[key] = np.empty(points.shape[0])

        self.s_dom = points[:, 0]
        self.u_dom = np.empty(points.shape[0])
        self.v_dom = np.empty(points.shape[0])

        for p, point in enumerate(points):
            s, u, v = point[0], point[1], point[2]

            # Construct VMEC Coordinate Mode Mesh #
            pol_xm_vmec = self.xm_vmec.reshape(self.md_vmec, 1) * u
            tor_xn_vmec = self.xn_vmec.reshape(self.md_vmec, 1) * v

            pol_xm_booz = self.xm_booz.reshape(self.md_booz, 1) * u
            tor_xn_booz = self.xn_booz.reshape(self.md_booz, 1) * v

            # Calculate Inverse VMEC Fourier Transforms #
            sin_mu_nv_vmec = np.sin(pol_xm_vmec - tor_xn_vmec)
            sin_mu_nv_booz = np.sin(pol_xm_booz - tor_xn_booz)

            Lambda = np.dot(self.lambda_amps(s), sin_mu_nv_vmec)[0]
            P = np.dot(self.fourierAmps['P'](s), sin_mu_nv_booz)[0]

            # Transform to Boozer Coordinates #
            u = u + Lambda - self.iota(s) * P
            v = v - P

            self.u_dom[p], self.v_dom[p] = u, v

            pol_xm = self.xm_booz.reshape(self.md_booz, 1) * u
            tor_xn = self.xn_booz.reshape(self.md_booz, 1) * v

            cos_mu_nv = np.cos(pol_xm - tor_xn)
            sin_mu_nv = np.sin(pol_xm - tor_xn)

            # Calculate Inverse Boozer Fourier Transforms #
            for key in ampKeys:

                fourAmps = self.fourierAmps[key](s)

                if any(ikey == key for ikey in self.cosine_keys):
                    self.invFourAmps[key][p] = np.dot(fourAmps, cos_mu_nv)[0]

                elif any(ikey == key for ikey in self.sine_keys):
                    self.invFourAmps[key][p] = np.dot(fourAmps, sin_mu_nv)[0]

                else:
                    raise NameError('key = {} : is not available'.format(key))

    def calc_metric_tensor(self, s_dom, u_dom, v_dom):
        """ Calculate the metric tensor components.
        """
        if self.space_derivs:
            s_num = len(s_dom)
            u_num = len(u_dom)
            v_num = len(v_dom)

            # Perform Fourier Transforms #
            ampKeys = ['R', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du',
                       'dZ_dv']

            if (s_num > 1) and (u_num > 1) and (v_num > 1):
                self.transForm_3D(s_dom, u_dom, v_dom, ampKeys)
            elif (s_num == 1) and (u_num > 1) and (v_num > 1):
                self.transForm_2D_sSec(s_dom[0], u_dom, v_dom, ampKeys)
            elif (s_num == 1) and (u_num == 1) and (v_num == 1):
                self.transForm_1D(s_dom[0], u_dom[0], v_dom[0], ampKeys)

            # Retrieve Coordinates and Derivatives #
            R = self.invFourAmps['R']

            dR_ds = self.invFourAmps['dR_ds']
            dR_du = self.invFourAmps['dR_du']
            dR_dv = self.invFourAmps['dR_dv']

            dZ_ds = self.invFourAmps['dZ_ds']
            dZ_du = self.invFourAmps['dZ_du']
            dZ_dv = self.invFourAmps['dZ_dv']

            # Calculate Covariant Metric Components #
            self.gss = dR_ds**2 + dZ_ds**2
            self.gsu = dR_ds * dR_du + dZ_ds * dZ_du
            self.gsv = dR_ds * dR_dv + dZ_ds * dZ_dv
            self.guu = dR_du**2 + dZ_du**2
            self.guv = dR_du * dR_dv + dZ_du * dZ_dv
            self.gvv = dR_dv**2 + dZ_dv**2 + R**2

            # Calculate Contravariant Metric Components #
            jacob_inv = 1. / (R * R * ((dR_du * dZ_ds - dR_ds * dZ_du)**2))
            self.g_ss = jacob_inv * ((dR_du*dZ_dv - dR_dv*dZ_du)**2
                                     + (R**2) * (dR_du**2 + dZ_du**2))
            self.g_uu = jacob_inv * ((dR_ds*dZ_dv - dR_dv*dZ_ds)**2
                                     + (R**2) * (dZ_ds**2 + dR_ds**2))
            self.g_vv = jacob_inv * ((dR_ds*dZ_du - dR_du*dZ_ds)**2)
            self.g_su = jacob_inv * ((dR_ds*dZ_dv - dR_dv*dZ_ds)
                                     * (dR_dv*dZ_du - dR_du*dZ_dv)
                                     - (R**2) * (dZ_ds*dZ_du + dR_ds*dR_du))
            self.g_sv = jacob_inv * ((dR_du*dZ_dv - dR_dv*dZ_du)
                                     * (dR_ds*dZ_du - dR_du*dZ_ds))
            self.g_uv = jacob_inv * ((dR_ds*dZ_dv - dR_dv*dZ_ds)
                                     * (dR_du*dZ_ds - dR_ds*dZ_du))

        else:
            raise KeyError("space_derivs flag must be set to True")

    def follow_field_line(self, s, alpha, nz, n_pol, ampKeys):
        """ Follow a magnetic field line following the GIST specification parameters.

        Parameters
        ----------
        s : float
            normalized toroidal flux magnetic surface label.  Mathces VMEC "s".
        alpha : float
            Clebsch representation field line label. Goes from 0 to 2pi.
        nz : int
            Number of grid points along field line.  GIST "z" coordinate.
        n_pol : float
            Number of poloidal rotations for field line following.
        """
        self.pol_dom = np.pi * np.linspace(-n_pol, n_pol, nz)
        self.tor_dom = (self.pol_dom - alpha) / self.iota(s)

        self.invFourAmps = {}
        for key in ampKeys:
            self.invFourAmps[key] = np.empty(nz)

        for i in range(nz):
            pol_xm = self.xm_booz.reshape(self.md_booz, 1) * self.pol_dom[i]
            tor_xn = self.xn_booz.reshape(self.md_booz, 1) * self.tor_dom[i]

            cos_mu_nv = np.cos(pol_xm - tor_xn)
            sin_mu_nv = np.sin(pol_xm - tor_xn)

            for key in ampKeys:

                fourAmps = self.fourierAmps[key](s)

                if any(ikey == key for ikey in self.cosine_keys):
                    self.invFourAmps[key][i] = np.dot(fourAmps, cos_mu_nv)[0]

                elif any(ikey == key for ikey in self.sine_keys):
                    self.invFourAmps[key][i] = np.dot(fourAmps, sin_mu_nv)[0]

                else:
                    raise NameError('key = {} : is not available'.format(key))

    def flux_tube_geometry(self, s0, a0, pram_file, ampKeys, Ninterp=0):
        """ Transform GENE flux-tube coordinates into Cartesian coordinates.

        Parameters
        ----------
        s0 : float
            Normalized flux-surface label that defines central field-line.
        a0 : float
            Field-line label that defines the central field_line.
        pram_file : str
            Absolute path to parameters file output by GENE.
        ampKeys : list
            List of quantities to be calculated.
        Ninterp : int (optional)
            Number of GENE x-y planes to be interpolated. Default is 0.
        """
        pram = rp.read_file(pram_file)

        lx = pram.data_dict['lx']
        ly = pram.data_dict['ly']
        nx0 = int(pram.data_dict['nx0'])
        nky0 = int(pram.data_dict['nky0'])
        nz0 = int(pram.data_dict['nz0'])

        Nx = nx0
        Ny = 2*nky0
        Nz = nz0 * (1 + Ninterp) - Ninterp

        q0 = pram.data_dict['q0']
        shat = pram.data_dict['shat']
        n_pol = pram.data_dict['n_pol']
        mref = const.m_p * pram.data_dict['mref']
        Tref = 1e3 * const.e * pram.data_dict['Tref']
        Lref = pram.data_dict['Lref']
        Bref = pram.data_dict['Bref']
        rho_ref = np.sqrt(mref*Tref)/(const.e*Bref)

        hlf_stp = np.pi * n_pol / (nz0 - 1)
        pol_dom = np.pi * np.linspace(-n_pol, n_pol, Nz)
        pol_dom = pol_dom - hlf_stp

        x_dom = 0.5 * lx * np.linspace(-1., (Nx-3)/(Nx-1), Nx)
        y_dom = 0.5 * ly * np.linspace(-1, (Ny-3)/(Ny-1), Ny)

        x_scale = (2*np.sqrt(s0)*rho_ref)/(q0*Lref)
        y_scale = (q0*rho_ref)/(np.sqrt(s0)*Lref)

        s_dom = x_scale * x_dom + s0
        s_dom = s_dom[(s_dom >= self.s_booz[0]) & (s_dom <= self.s_booz[-1])]
        x_dom = s_dom[(s_dom >= self.s_booz[0]) & (s_dom <= self.s_booz[-1])]
        q_rng = q0*(1 + (s_dom - s0)*(shat/(2*s0)))

        Ny = y_dom.shape[0]
        Nx = x_dom.shape[0]

        cart_points = np.empty((Nz, Ny, Nx, 4))
        cart_points[:, :, :, :] = np.nan
        for pol_idx, pol in enumerate(pol_dom):
            print('theta = {0:0.4f} ({1:0.0f}|{2:0.0f})'.format(pol, pol_idx+1, Nz))

            self.invFourAmps = {}
            for key in ampKeys:
                self.invFourAmps[key] = np.empty((Ny, Nx))
                self.invFourAmps[key][:, :] = np.nan

            for ydx, y in enumerate(y_dom):
                for sdx, s in enumerate(s_dom):
                    tor = y_scale * y + q_rng[sdx]*pol - a0
                    pol_xm = self.xm_booz.reshape(self.md_booz, 1) * pol
                    tor_xn = self.xn_booz.reshape(self.md_booz, 1) * tor

                    cos_mu_nv = np.cos(pol_xm - tor_xn)
                    sin_mu_nv = np.sin(pol_xm - tor_xn)

                    for key in ampKeys:

                        if key == 'Phi':
                            P = np.dot(self.fourierAmps['P'](s), sin_mu_nv)[0]
                            self.invFourAmps[key][ydx, sdx] = tor + P

                        else:
                            fourAmps = self.fourierAmps[key](s)

                            if any(ikey == key for ikey in self.cosine_keys):
                                self.invFourAmps[key][ydx, sdx] = np.dot(fourAmps, cos_mu_nv)[0]

                            elif any(ikey == key for ikey in self.sine_keys):
                                self.invFourAmps[key][ydx, sdx] = np.dot(fourAmps, sin_mu_nv)[0]

                            else:
                                raise NameError('key = {} : is not available'.format(key))

            X = self.invFourAmps['R'] * np.cos(self.invFourAmps['Phi'])
            Y = self.invFourAmps['R'] * np.sin(self.invFourAmps['Phi'])

            cart_points[pol_idx, :, :, 0] = X
            cart_points[pol_idx, :, :, 1] = Y
            cart_points[pol_idx, :, :, 2] = self.invFourAmps['Z']
            cart_points[pol_idx, :, :, 3] = self.invFourAmps['Bmod']

        self.cart_points = cart_points

        self.gene_x = x_dom
        self.gene_y = y_dom
        self.pol_dom = pol_dom

        self.q0 = q0
        self.s0 = s0
        self.alpha0 = a0
        self.shat = shat
        self.Lref = Lref
        self.Bref = Bref
        self.rho_ref = rho_ref

    def plot_spectrum(self, plot, nSpec=10, excFirst=True, savePath=None):
        """ Plots the most prominant modes according to the Fourier amplitude
        specified.  The first mode is excluded by default.

        Parameters
        ----------
        ax : object
            axis on which to plot the spectrum
        ampKey : str
            key specifying Fourier amplitude to be plotted
        nSpec : int
            number of modes to plot (default is 10)
        excFirst : boolean
            defaults to exclude the first mode (default is True)
        """
        specData = {}
        specOrder = []

        s_norm = 1. / self.s_booz[-1] - self.s_booz[0]
        for i in range(0, self.md_booz):
            key = '[{0}, {1}]'.format(int(self.xn_booz[i]), int(self.xm_booz[i]))
            spec = self.fourierAmps['Bmod'](self.s_booz)[:, i]
            specMax = np.trapz(np.abs(spec), self.s_booz) * s_norm

            specData[key] = spec
            specOrder.append([specMax, key])

        if excFirst:
            io = 1
        else:
            io =0

        specOrder = sorted(specOrder, key=lambda x: x[0], reverse=True)

        specMin = np.inf
        specMax = -np.inf
        for i in range(nSpec):
            key = specOrder[io+i][1]
            spec = specData[key]

            plot.plt.plot(self.s_booz, spec, label=key)

            if np.max(spec) > specMax:
                specMax = np.max(spec)
            if np.min(spec) < specMin:
                specMin = np.min(spec)

        specMin = 1.1 * specMin
        specMax = 1.1 * specMax

        # plot.plt.plot([0.25, 0.25], [specMin, specMax], c='k', ls='--', lineWidth=1)

        plot.plt.xlabel(r'$\Psi_N$')
        plot.plt.ylabel(r'$B_{nm}$')

        plot.plt.xlim(0, 1)
        plot.plt.ylim(specMin, specMax)

        box = plot.ax.get_position()
        plot.ax.set_position([box.x0, box.y0, box.width * 1, box.height])

        plot.plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1))

        plot.plt.grid()
        plot.plt.tight_layout()

        if savePath is None:
            plot.plt.show()
        else:
            plot.plt.savefig(savePath)


if __name__ == "__main__":
    # Full path to boozmn file #
    # Full path to vmec wout file #
    # Full path to output GENE parameters file #
    booz_path = os.path.join(os.getcwd(), 'boozmn_wout_HSX_main_opt0.nc')
    wout_path = os.path.join(os.getcwd(), 'wout_HSX_main_opt0.nc')
    pram_path = os.path.join(os.getcwd(), 'parameters')

    # Define normalized flux-surface label (psi) #
    # Define field-line label (alpha) #
    # Specify variable that will be transformed #
    # Specify number of interpolated GENE x-y planes #
    s0 = 0.5
    alf0 = 0.0
    ampKeys = ['R', 'Z', 'Phi', 'Bmod']
    Ninterp = 3

    # Perform transformation from GENE coordinates to Cartesian coordinates #
    booz = readBooz(booz_path, wout_path)
    booz.flux_tube_geometry(s0, alf0, pram_path, ampKeys, Ninterp=Ninterp)

    # Save transformation data #
    hf_path = os.path.join(os.getcwd(), 'flux_tube_data.h5')
    with hf.File(hf_path, 'w') as hf_:
        hf_.create_dataset('GENE x domain', data=booz.gene_x)
        hf_.create_dataset('GENE y domain', data=booz.gene_y)
        hf_.create_dataset('cartesian coordinates', data=booz.cart_points)
        hf_.create_dataset('poloidal domain', data=booz.pol_dom)
        hf_.create_dataset('safety factor', data=booz.safty_factor)
        hf_.create_dataset('central s', data=booz.s0)
        hf_.create_dataset('central alpha', data=booz.alpha0)
        hf_.create_dataset('Bref', data=booz.Bref)
        hf_.create_dataset('Lref', data=booz.Lref)
