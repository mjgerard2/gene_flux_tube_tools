import os
import h5py as hf
import numpy as np


def scalar_mesh(path, name, cartGrid, scalarField):
    """ Generates a vtk file for an unstructured mesh with a scalar field

    Parameters
    ----------
    path : str
        Directory path where vtk files will be saved.
    name : atr
        Name of vtk file to be saved.
    cartGrid : array
        x, y and z coordinates on unstructured grid.
    scalarField : array
        Scalar values at each x, y and z coordinate grid point.
    """
    n = cartGrid.shape[0] - 1
    m = cartGrid.shape[1] - 1
    uv_num = n * m

    xGrid = cartGrid[:, :, 0]
    yGrid = cartGrid[:, :, 1]
    zGrid = cartGrid[:, :, 2]

    P = np.empty((uv_num, 3))
    E_cell = np.empty((uv_num, 3))
    S_cell = np.empty((uv_num, 3))
    for l in range(m):
        for k in range(n):
            idx = l * (n) + k

            x, y, z = xGrid[k, l], yGrid[k, l], zGrid[k, l]
            P[idx] = x, y, z

            e1 = l*(n) + k
            e2 = (((l+1)*(n)) % ((m)*(n))) + ((k+1) % (n))
            e3 = l * (n) + (k+1) % (n)
            E_cell[idx] = e1, e2, e3

            s1 = l*(n) + k
            s2 = ((l+1) * (n) + k) % ((m) * (n))
            s3 = ((l+1) * (n)) % ((m) * (n)) + (k+1) % (n)
            S_cell[idx] = s1, s2, s3

    with open(os.path.join(path, name), 'w') as file:
        file.write('# vtk DataFile Version 3.0\n' +
                   'QHS configuration, vector B field on Unstructured Mesh\n' +
                   'ASCII\n\n' +
                   'DATASET UNSTRUCTURED_GRID\n' +
                   'POINTS {} float\n'.format(int(uv_num)))

        for i in range(uv_num):
            file.write('{0} {1} {2}\n'.format(float(P[i][0]), float(P[i][1]), float(P[i][2])))

        file.write('\n' +
                   'CELLS {0} {1}\n'.format(int(2*uv_num), int(8*uv_num)))

        for i in range(uv_num):
            file.write('3 {0} {1} {2}\n'.format(int(E_cell[i][0]), int(E_cell[i][1]), int(E_cell[i][2])))
            file.write('3 {0} {1} {2}\n'.format(int(S_cell[i][0]), int(S_cell[i][1]), int(S_cell[i][2])))

        file.write('\n' +
                   'CELL_TYPES {0}\n'.format(int(2*uv_num)))

        for i in range(2*uv_num):
            file.write('6\n')

        file.write('\n' +
                   'POINT_DATA {}\n'.format(int(uv_num)) +
                   'SCALARS temperature  float 1\n' +
                   'LOOKUP_TABLE default\n')

        for l in range(m):
            for k in range(n):
                scalar = scalarField[k, l]
                file.write('{0}\n'.format(scalar))


def vector_mesh(path, name, cartGrid, vectorField):
    """ Generates a vtk file for an unstructured mesh with a vector field

    Parameters
    ----------
    path : str
        Directory path where vtk files will be saved.
    name : atr
        Name of vtk file to be saved.
    cartGrid : array
        x, y and z coordinates on unstructured grid.
    vectorField : array
        Vector values at each x, y and z coordinate grid point.
    """
    n = cartGrid.shape[0] - 1
    m = cartGrid.shape[1] - 1
    uv_num = n * m

    xGrid = cartGrid[:, :, 0]
    yGrid = cartGrid[:, :, 1]
    zGrid = cartGrid[:, :, 2]

    P = np.empty((uv_num, 3))
    E_cell = np.empty((uv_num, 3))
    S_cell = np.empty((uv_num, 3))
    for l in range(m):
        for k in range(n):
            idx = l * (n) + k

            x, y, z = xGrid[k, l], yGrid[k, l], zGrid[k, l]
            P[idx] = x, y, z

            e1 = l*(n) + k
            e2 = (((l+1)*(n)) % ((m)*(n))) + ((k+1) % (n))
            e3 = l * (n) + (k+1) % (n)
            E_cell[idx] = e1, e2, e3

            s1 = l*(n) + k
            s2 = ((l+1) * (n) + k) % ((m) * (n))
            s3 = ((l+1) * (n)) % ((m) * (n)) + (k+1) % (n)
            S_cell[idx] = s1, s2, s3

    file = open(os.path.join(path, name), 'w')
    file.write('# vtk DataFile Version 3.0\n' +
               'QHS configuration, vector B field on Unstructured Mesh\n' +
               'ASCII\n\n' +
               'DATASET UNSTRUCTURED_GRID\n' +
               'POINTS {} float\n'.format(int(uv_num)))

    for i in range(uv_num):
        file.write('{0} {1} {2}\n'.format(float(P[i][0]), float(P[i][1]), float(P[i][2])))

    file.write('\n' +
               'CELLS {0} {1}\n'.format(int(2*uv_num), int(8*uv_num)))

    for i in range(uv_num):
        file.write('3 {0} {1} {2}\n'.format(int(E_cell[i][0]), int(E_cell[i][1]), int(E_cell[i][2])))
        file.write('3 {0} {1} {2}\n'.format(int(S_cell[i][0]), int(S_cell[i][1]), int(S_cell[i][2])))

    file.write('\n' +
               'CELL_TYPES {0}\n'.format(int(2*uv_num)))

    for i in range(2*uv_num):
        file.write('6\n')

    file.write('\n' +
               'POINT_DATA {}\n'.format(int(uv_num)) +
               'VECTORS curvature float\n')

    for l in range(m):
        for k in range(n):
            vec = vectorField[k, l]
            file.write('{0} {1} {2}\n'.format(vec[0], vec[1], vec[2]))

    file.close()


def scalar_line_mesh(path, name, cartGrid, scalarField):
    """ Generates a vtk file for a scalar field along a 3D line.

    Parameters
    ----------
    path : str
        Directory path where vtk files will be saved.
    name : atr
        Name of vtk file to be saved.
    cartGrid : array
        x, y and z coordinates on unstructured grid.
    scalarField : array
        Scalar values at each x, y and z coordinate grid point.
    """
    gpts = int(scalarField.shape[0])

    Pnts = np.empty((gpts, 3))
    Cell = np.empty((gpts, 2))
    for idx, x in enumerate(scalarField):
        Pnts[idx] = cartGrid[idx]
        Cell[idx] = idx, idx+1

    file = open(os.path.join(path, name), 'w')
    file.write('# vtk DataFile Version 3.0\n' +
               'QHS configuration, vector B field on Unstructured Mesh\n' +
               'ASCII\n\n' +
               'DATASET UNSTRUCTURED_GRID\n' +
               'POINTS {} float\n'.format(gpts))

    for g in range(gpts):
        file.write('{0} {1} {2}\n'.format(Pnts[g][0], Pnts[g][1], Pnts[g][2]))

    file.write('\n' +
               'CELLS {0} {1}\n'.format(gpts-1, 3*(gpts-1)))

    for g in range(gpts-1):
        file.write('2 {0} {1}\n'.format(int(Cell[g][0]), int(Cell[g][1])))

    file.write('\n' +
               'CELL_TYPES {0}\n'.format(gpts-1))

    for g in range(gpts-1):
        file.write('4\n')

    file.write('\n' +
               'POINT_DATA {}\n'.format(gpts) +
               'SCALARS temperature  float 1\n' +
               'LOOKUP_TABLE default\n')

    for g in range(gpts):
        scalar = scalarField[g]
        file.write('{0}\n'.format(scalar))

    file.close()


def vector_line_mesh(path, name, cartGrid, vectorField):
    """ Generates a vtk file for a vector field along a 3D line.

    Parameters
    ----------
    path : str
        Directory path where vtk files will be saved.
    name : atr
        Name of vtk file to be saved.
    cartGrid : array
        x, y and z coordinates on unstructured grid.
    vectorField : array
        Scalar values at each x, y and z coordinate grid point.
    """
    gpts = int(vectorField.shape[0])

    Pnts = np.empty((gpts, 3))
    Cell = np.empty((gpts, 2))
    for idx, x in enumerate(vectorField):
        Pnts[idx] = cartGrid[idx]
        Cell[idx] = idx, idx+1

    file = open(os.path.join(path, name), 'w')
    file.write('# vtk DataFile Version 3.0\n' +
               'QHS configuration, vector B field on Unstructured Mesh\n' +
               'ASCII\n\n' +
               'DATASET UNSTRUCTURED_GRID\n' +
               'POINTS {} float\n'.format(gpts))

    for g in range(gpts):
        file.write('{0} {1} {2}\n'.format(Pnts[g][0], Pnts[g][1], Pnts[g][2]))

    file.write('\n' +
               'CELLS {0} {1}\n'.format(gpts-1, 3*(gpts-1)))

    for g in range(gpts-1):
        file.write('2 {0} {1}\n'.format(int(Cell[g][0]), int(Cell[g][1])))

    file.write('\n' +
               'CELL_TYPES {0}\n'.format(gpts-1))

    for g in range(gpts-1):
        file.write('4\n')

    file.write('\n' +
               'POINT_DATA {}\n'.format(gpts) +
               'VECTORS curvature float\n')

    for g in range(gpts):
        vec = vectorField[g]
        file.write('{0} {1} {2}\n'.format(vec[0], vec[1], vec[2]))

    file.close()


def flux_tube_field_line(flux_path, vtk_path, pol_beg=-np.pi, pol_end=np.pi, x_val=0., y_val=0.):
    """ Generate a vtk file for the central field-line in a GENE flux-tube on
    an unstructured mesh with a scalar field

    Parameters
    ----------
    flux_path : str
        Absolute path to the flux-tube data file.
    vtk_path : str
        Absolute path to vtk that will be saved.
    pol_beg : float (optional)
        Poloidal angle at which flux-tube domain begins. Default is -np.pi.
    pol_end : float (optional)
        Poloidal angle at which flux-tube domain ends. Default is np.pi.
    """
    with hf.File(flux_path, 'r') as hf_:
        x_dom = hf_['GENE x domain'][()]
        y_dom = hf_['GENE y domain'][()]
        pol_dom = hf_['poloidal domain'][()]
        cart_data = hf_['cartesian coordinates'][()]

    if pol_beg < pol_dom[0]:
        raise ValueError('Beginning poloidal angle {} is outside the flux-tube domain.'.format(pol_beg))
    if pol_end > pol_dom[-1]:
        raise ValueError('Ending poloidal angle {} is outside the flux-tube domain.'.format(pol_end))

    x_idx = np.argmin(np.abs(x_dom - x_val))
    y_idx = np.argmin(np.abs(y_dom - y_val))

    beg = np.argmin(np.abs(pol_dom - pol_beg))
    end = np.argmin(np.abs(pol_dom - pol_end))

    cart_data = cart_data[beg:end+1, y_idx, x_idx, :]
    npts = cart_data.shape[0]

    file = open(vtk_path, 'w')
    file.write('# vtk DataFile Version 3.0\n' +
               'QHS configuration, vector B field on Unstructured Mesh\n' +
               'ASCII\n\n' +
               'DATASET UNSTRUCTURED_GRID\n' +
               'POINTS {} float\n'.format(npts))

    for cart in cart_data:
        x, y, z, Bmod = cart
        file.write('{0} {1} {2}\n'.format(x, y, z))

    file.write('\n' +
               'CELLS {0} {1}\n'.format(npts-1, 3*(npts-1)))

    for i in range(npts-1):
        file.write('2 {0} {1}\n'.format(int(i), int(i+1)))

    file.write('\n' +
               'CELL_TYPES {0}\n'.format(npts-1))

    for i in range(npts-1):
        file.write('4\n')

    file.write('\n' +
               'POINT_DATA {}\n'.format(npts) +
               'SCALARS temperature  float 1\n' +
               'LOOKUP_TABLE default\n')

    for cart in cart_data:
        x, y, z, Bmod = cart
        file.write('{}\n'.format(Bmod))

    file.close()


def flux_tube_planes(flux_path, vtk_path, pol_beg=-np.pi, pol_end=np.pi, scalar_data=None):
    """ Generate a vtk file for the psi-alpha planes along a GENE flux-tube on
    an unstructured mesh with a scalar field

    Parameters
    ----------
    flux_path : str
        Absolute path to the flux-tube data file.
    vtk_path : str
        Absolute path to vtk that will be saved.
    pol_beg : float (optional)
        Poloidal angle at which flux-tube domain begins. Default is -np.pi.
    pol_end : float (optional)
        Poloidal angle at which flux-tube domain ends. Default is np.pi.
    """
    with hf.File(flux_path, 'r') as hf_:
        pol_dom = hf_['poloidal domain'][()]
        cart_data = hf_['cartesian coordinates'][()]
        print(cart_data.shape)

    if pol_beg < pol_dom[0]:
        raise ValueError('Beginning poloidal angle {} is outside the flux-tube domain.'.format(pol_beg))
    if pol_end > pol_dom[-1]:
        raise ValueError('Ending poloidal angle {} is outside the flux-tube domain.'.format(pol_end))

    beg = np.argmin(np.abs(pol_dom - pol_beg))
    end = np.argmin(np.abs(pol_dom - pol_end))
    cart_data = cart_data[beg:end+1, :, :, :]

    Np, Ny, Nx = cart_data.shape[0], cart_data.shape[1], cart_data.shape[2]
    NyNx = Ny*Nx
    num_of_points = int(Np*Ny*Nx)
    num_of_cells = int(Np*(Ny-1))

    with open(vtk_path, 'w') as f:
        f.write('# vtk DataFile Version 3.0\n' +
                'Flux Tube Rendering. Data f: '+flux_path+'\n' +
                'ASCII\n\n' +
                'DATASET UNSTRUCTURED_GRID\n' +
                'POINTS {} float\n'.format(num_of_points))

        for i in range(Np):
            for j in range(Ny):
                for k in range(Nx):
                    x, y, z = cart_data[i, j, k, 0:3]
                    f.write('{0} {1} {2}\n'.format(x, y, z))

        f.write('\n' +
                'CELLS {0} {1}\n'.format(num_of_cells, (2*Nx+1)*num_of_cells))

        for i in range(Np):
            for j in range(Ny-1):
                C0 = NyNx*i + Nx*j
                C_string = '{0:0.0f} '.format(2*Nx)+' '.join(['{0:0.0f} {1:0.0f}'.format(C0+k, C0+Nx+k) for k in range(Nx)])+'\n'
                f.write(C_string)

        f.write('\n' +
                'CELL_TYPES {0}\n'.format(num_of_cells))

        for i in range(num_of_cells):
            f.write('6\n')

        f.write('\n' +
                'POINT_DATA {}\n'.format(num_of_points) +
                'SCALARS temperature  float 1\n' +
                'LOOKUP_TABLE default\n')

        if scalar_data is None:
            for i in range(Np):
                for j in range(Ny):
                    for k in range(Nx):
                        scalar = cart_data[i, j, k, 3]
                        f.write('{0}\n'.format(scalar))
        else:
            for i in range(Np):
                for j in range(Ny):
                    for k in range(Nx):
                        scalar = scalar_data[i, j, k]
                        f.write('{0}\n'.format(scalar))


def flux_tube_volume(flux_path, vtk_path, pol_beg=-np.pi, pol_end=np.pi, volume_element='VTK_HEX', scalar_data=None):
    """ Generate a vtk file for a GENE flux-tube volume on an unstructured
    mesh with a scalar field

    Parameters
    ----------
    flux_path : str
        Absolute path to the flux-tube data file.
    vtk_path : str
        Absolute path to vtk that will be saved.
    pol_beg : float (optional)
        Poloidal angle at which flux-tube domain begins. Default is -np.pi.
    pol_end : float (optional)
        Poloidal angle at which flux-tube domain ends. Default is np.pi.
    volume_element : str (optional)
        Define unit volume element. The options are VTK_TETRA, VTK_WEDGE, and
        VTK_HEX. Default is VTK_HEX.
    scalar_data : arr (optional)
        Scalar data to be plotted on unstructured mesh. Defaut is None, which
        results in a |B| mesh.
    """
    with hf.File(flux_path, 'r') as hf_:
        pol_dom = hf_['poloidal domain'][()]
        cart_data = hf_['cartesian coordinates'][()]

    if pol_beg < pol_dom[0]:
        raise ValueError('Beginning poloidal angle {} is outside the flux-tube domain.'.format(pol_beg))
    if pol_end > pol_dom[-1]:
        raise ValueError('Ending poloidal angle {} is outside the flux-tube domain.'.format(pol_end))

    beg = np.argmin(np.abs(pol_dom - pol_beg))
    end = np.argmin(np.abs(pol_dom - pol_end))
    cart_data = cart_data[beg:end+1, :, :, :]

    Np, Ny, Nx = cart_data.shape[0], cart_data.shape[1]-1, cart_data.shape[2]-1
    NxNy = int(Nx*Ny)
    num_of_points = int(Np*NxNy)

    if volume_element == "VTK_HEX":
        num_of_cells = int((Np-1)*(Nx-1)*(Ny-1))
    elif volume_element == "VTK_WEDGE":
        num_of_cells = 2*int((Np-1)*(Nx-1)*(Ny-1))
    elif volume_element == "VTK_TETRA":
        num_of_cells = 6*int((Np-1)*(Nx-1)*(Ny-1))
    else:
        raise KeyError(volume_element+" is not a valid volume element.")

    with open(vtk_path, 'w') as f:
        f.write('# vtk DataFile Version 3.0\n' +
                'Flux Tube Rendering. Data f: '+flux_path+'\n' +
                'ASCII\n\n' +
                'DATASET UNSTRUCTURED_GRID\n' +
                'POINTS {} float\n'.format(num_of_points))

        for i in range(Np):
            for j in range(Ny):
                for k in range(Nx):
                    x, y, z = cart_data[i, j, k, 0:3]
                    f.write('{0} {1} {2}\n'.format(x, y, z))

        if volume_element == "VTK_HEX":
            f.write('\n' +
                    'CELLS {0} {1}\n'.format(num_of_cells, 9*num_of_cells))

            for i in range(Np-1):
                for j in range(Ny-1):
                    for k in range(Nx-1):
                        C0 = int(Nx*Ny*i + Nx*j + k)
                        C1 = C0+1
                        C2 = C0+Nx+1
                        C3 = C0+Nx
                        C4 = C0+Nx*Ny
                        C5 = C0+Nx*Ny+1
                        C6 = C0+Nx*Ny+Nx+1
                        C7 = C0+Nx*Ny+Nx
                        f.write('8 {0} {1} {2} {3} {4} {5} {6} {7}\n'.format(C0, C1, C2, C3, C4, C5, C6, C7))

        elif volume_element == "VTK_WEDGE":
            f.write('\n' +
                    'CELLS {0} {1}\n'.format(num_of_cells, 7*num_of_cells))

            for i in range(Np-1):
                for j in range(Ny-1):
                    for k in range(Nx-1):
                        base_idx = NxNy*i + Nx*j + k
                        C0 = base_idx + np.array([0, Nx, 1, NxNy, NxNy+Nx, NxNy+1], dtype=int)
                        C1 = base_idx + np.array([Nx+1, 1, Nx, NxNy+Nx+1, NxNy+1, NxNy+Nx], dtype=int)

                        f.write('6 {0} {1} {2} {3} {4} {5}\n'.format(C0[0], C0[1], C0[2], C0[3], C0[4], C0[5]))
                        f.write('6 {0} {1} {2} {3} {4} {5}\n'.format(C1[0], C1[1], C1[2], C1[3], C1[4], C1[5]))

        elif volume_element == "VTK_TETRA":
            f.write('\n' +
                    'CELLS {0} {1}\n'.format(num_of_cells, 5*num_of_cells))

            for i in range(Np-1):
                for j in range(Ny-1):
                    for k in range(Nx-1):
                        C0 = NxNy*i + Nx*j + k

                        C1 = C0 + np.array([1, 0, Nx, NxNy], dtype=int)
                        C2 = C0 + np.array([Nx, 1, NxNy, NxNy+1], dtype=int)
                        C3 = C0 + np.array([NxNy+1, NxNy+Nx, NxNy, Nx], dtype=int)

                        C4 = C0 + np.array([NxNy+1, NxNy+Nx+1, NxNy+Nx, Nx+1], dtype=int)
                        C5 = C0 + np.array([Nx+1, 1, Nx, NxNy+1], dtype=int)
                        C6 = C0 + np.array([NxNy+1, Nx, Nx+1, NxNy+Nx], dtype=int)

                        f.write('4 {0} {1} {2} {3}\n'.format(C1[0], C1[1], C1[2], C1[3]))
                        f.write('4 {0} {1} {2} {3}\n'.format(C2[0], C2[1], C2[2], C2[3]))
                        f.write('4 {0} {1} {2} {3}\n'.format(C3[0], C3[1], C3[2], C3[3]))

                        f.write('4 {0} {1} {2} {3}\n'.format(C4[0], C4[1], C4[2], C4[3]))
                        f.write('4 {0} {1} {2} {3}\n'.format(C5[0], C5[1], C5[2], C5[3]))
                        f.write('4 {0} {1} {2} {3}\n'.format(C6[0], C6[1], C6[2], C6[3]))

        f.write('\n' +
                'CELL_TYPES {0}\n'.format(num_of_cells))

        if volume_element == "VTK_HEX":
            for i in range(num_of_cells):
                f.write('12\n')

        elif volume_element == "VTK_WEDGE":
            for i in range(num_of_cells):
                f.write('13\n')

        elif volume_element == "VTK_TETRA":
            for i in range(num_of_cells):
                f.write('10\n')

        f.write('\n' +
                'POINT_DATA {}\n'.format(num_of_points) +
                'SCALARS temperature  float 1\n' +
                'LOOKUP_TABLE default\n')

        if scalar_data is None:
            for i in range(Np):
                for j in range(Ny):
                    for k in range(Nx):
                        scalar = cart_data[i, j, k, 3]
                        f.write('{0}\n'.format(scalar))
        else:
            for i in range(Np):
                for j in range(Ny):
                    for k in range(Nx):
                        scalar = scalar_data[i, j, k]
                        f.write('{0}\n'.format(scalar))


def coil_mesh(path, name, coil, crnt, segs=60, pack=14, leng=.130, width=.056):
    idx_steps = np.array([int((segs)*i) for i in range(pack)], dtype=np.int)

    npts = segs * pack

    P = np.empty((npts, 3))
    S_cell = np.empty((npts, 4))
    for s in range(segs):
        steps = s + idx_steps
        xGrid, yGrid, zGrid = coil[steps, 0], coil[steps, 1], coil[steps, 2]
        for p in range(pack):
            idx = p + s * pack
            P[idx] = xGrid[p], yGrid[p], zGrid[p]

            s1 = idx
            s2 = s * pack + ((p + 1) % pack)
            s3 = ((s+1) * pack + ((p+1) % pack)) % npts
            s4 = (pack * (s+1) + p) % npts
            S_cell[idx] = s1, s2, s3, s4

    with open(os.path.join(path, name), 'w') as file:
        file.write('# vtk DataFile Version 3.0\n' +
                   'QHS configuration, vector B field on Unstructured Mesh\n' +
                   'ASCII\n\n' +
                   'DATASET UNSTRUCTURED_GRID\n' +
                   'POINTS {} float\n'.format(int(npts)))

        for i in range(npts):
            file.write('{0} {1} {2}\n'.format(float(P[i][0]), float(P[i][1]), float(P[i][2])))

        file.write('\n' +
                   'CELLS {0} {1}\n'.format(int(npts), int(5*npts)))

        for i in range(npts):
            file.write('4 {0} {1} {2} {3}\n'.format(int(S_cell[i][0]), int(S_cell[i][1]), int(S_cell[i][2]), int(S_cell[i][3])))

        file.write('\n' +
                   'CELL_TYPES {0}\n'.format(int(npts)))

        for i in range(npts):
            file.write('9\n')

        file.write('\n' +
                   'POINT_DATA {}\n'.format(int(npts)) +
                   'SCALARS temperature  float 1\n' +
                   'LOOKUP_TABLE default\n')

        for l in range(npts):
            file.write('{0}\n'.format(crnt))

def aux_coil_mesh(path, name, coil, crnt, leng=.048, width=.019, npts=100):
    from scipy.interpolate import interp1d

    aux_mean = np.mean(coil, axis=0)

    aux_1 = coil[0] - aux_mean
    aux_1 = aux_1 / np.linalg.norm(aux_1)

    aux_2 = coil[int(.25*coil.shape[0])] - aux_mean
    aux_2 = aux_2 - np.dot(aux_1, aux_2) * aux_1
    aux_2 = aux_2 / np.linalg.norm(aux_2)

    theta_dom = np.empty(coil.shape[0])
    for a, aux in enumerate(coil[0:-1]):
        vec = aux - aux_mean

        e1 = np.dot(vec, aux_1)
        e2 = np.dot(vec, aux_2)

        theta_dom[a] = (np.arctan2(e2, e1) + 2*np.pi) % (2*np.pi)
    theta_dom[-1] = 2 * np.pi

    aux_interp_x = interp1d(theta_dom, coil[:, 0])
    aux_interp_y = interp1d(theta_dom, coil[:, 1])
    aux_interp_z = interp1d(theta_dom, coil[:, 2])

    theta_new = np.linspace(0, 2*np.pi, npts)

    aux_x = aux_interp_x(theta_new)
    aux_y = aux_interp_y(theta_new)
    aux_z = aux_interp_z(theta_new)

    aux_new = np.stack((aux_x, aux_y, aux_z), axis=1)

    aux_tan = np.roll(aux_new, 1, axis=0) - np.roll(aux_new, -1, axis=0)
    aux_tan_norm = np.linalg.norm(aux_tan, axis=1)

    aux_tan_x = aux_tan[:, 0] / aux_tan_norm
    aux_tan_y = aux_tan[:, 1] / aux_tan_norm
    aux_tan_z = aux_tan[:, 2] / aux_tan_norm

    aux_tan = np.stack((aux_tan_x, aux_tan_y, aux_tan_z), axis=1)

    Npts = 6 * npts
    P = np.empty((Npts, 3))
    S_cell = np.empty((Npts, 4))
    for a, aux in enumerate(aux_new):
        vec = aux - aux_mean

        aux_bin = np.cross(vec, aux_tan[a])
        aux_bin = aux_bin / np.linalg.norm(aux_bin)
        aux_nrm = np.cross(aux_tan[a], aux_bin)

        P[a*6] = aux + 0.5 * (leng * aux_nrm + width * aux_bin)
        P[a*6+1] = aux + 0.5 * width * aux_bin
        P[a*6+2] = aux - 0.5 * (leng * aux_nrm - width * aux_bin)
        P[a*6+3] = aux - 0.5 * (leng * aux_nrm + width * aux_bin)
        P[a*6+4] = aux - 0.5 * width * aux_bin
        P[a*6+5] = aux + 0.5 * (leng * aux_nrm - width * aux_bin)

        for b in range(6):
            idx = b + a * 6

            s1 = idx
            s2 = a * 6 + ((b + 1) % 6)
            s3 = ((a+1) * 6 + ((b+1) % 6)) % Npts
            s4 = (6 * (a+1) + b) % Npts
            S_cell[idx] = s1, s2, s3, s4

    with open(os.path.join(path, name), 'w') as file:
        file.write('# vtk DataFile Version 3.0\n' +
                   'QHS configuration, vector B field on Unstructured Mesh\n' +
                   'ASCII\n\n' +
                   'DATASET UNSTRUCTURED_GRID\n' +
                   'POINTS {} float\n'.format(int(Npts)))

        for i in range(Npts):
            file.write('{0} {1} {2}\n'.format(float(P[i][0]), float(P[i][1]), float(P[i][2])))

        file.write('\n' +
                   'CELLS {0} {1}\n'.format(int(Npts), int(5*Npts)))

        for i in range(Npts):
            file.write('4 {0} {1} {2} {3}\n'.format(int(S_cell[i][0]), int(S_cell[i][1]), int(S_cell[i][2]), int(S_cell[i][3])))

        file.write('\n' +
                   'CELL_TYPES {0}\n'.format(int(Npts)))

        for i in range(Npts):
            file.write('9\n')

        file.write('\n' +
                   'POINT_DATA {}\n'.format(int(Npts)) +
                   'SCALARS temperature  float 1\n' +
                   'LOOKUP_TABLE default\n')

        for l in range(Npts):
            file.write('{0}\n'.format(crnt))
