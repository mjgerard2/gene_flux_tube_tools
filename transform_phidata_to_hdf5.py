import os
import h5py as hf
import numpy as np
import read_parameters as rp

# Full path to GENE output parameters file #
# Full path to GENE output phidata file #
# Full path to transformed phidata file that will be generated #
pram_path = os.path.join(os.getcwd(), 'parameters')
phi_path = os.path.join(os.getcwd(), 'phidata.dat')
phi_hf_path = os.path.join(os.getcwd(), 'phidata.h5')

# Read parameters file #
pram = rp.read_file(pram_path)

Nx = int(pram.data_dict['nx0'])
Ny = 2*int(pram.data_dict['nky0'])
Nz = int(pram.data_dict['nz0'])

itr = 0
Nxy = Nx*Ny
Nxyz = Nxy*Nz

# Loop through phidata file and populate hdf5 file #
with open(phi_path, 'r') as f:
    while True:
        print('iteration: {}'.format(itr))
        try:
            line_set = [next(f) for x in range(Nxyz)]
            phi = np.empty((Nz, Ny, Nx))
            for i in range(Nz):
                for j in range(Ny):
                    for k in range(Nx):
                        ldx = i*Nxy + j*Nx + k
                        phi[i, j, k] = float(line_set[ldx].strip())

            with hf.File(phi_hf_path, 'a') as hf_:
                hf_.create_dataset('time {}'.format(itr), data=phi)
            itr += 1

        except StopIteration:
            break
