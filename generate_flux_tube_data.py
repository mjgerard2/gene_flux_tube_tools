import os
import h5py as hf
import read_boozmn as rb

# Full path to boozmn file #
# Full path to vmec wout file #
# Full path to output GENE parameters file #
# Full path to flux tube data file that will be generated #
booz_path = os.path.join(os.getcwd(), 'boozmn_wout_QHS_best.nc')
wout_path = os.path.join(os.getcwd(), 'wout_QHS_best.nc')
pram_path = os.path.join(os.getcwd(), 'parameters')
tube_path = os.path.join(os.getcwd(), 'flux_tube_data.h5')

# Define normalized flux-surface label (psi) #
# Define field-line label (alpha) #
# Specify variable that will be transformed #
s0 = 0.5
alf0 = 0.0
ampKeys = ['R', 'Z', 'Phi', 'Bmod']

# Specify the number of interpolated GENE #
# x-y planes between each simulated x-y plane #
Ninterp = 3

# Perform transformation from GENE coordinates to Cartesian coordinates #
booz = rb.readBooz(booz_path, wout_path)
booz.flux_tube_geometry(s0, alf0, pram_path, ampKeys, Ninterp=Ninterp)

# Save transformation data #
with hf.File(tube_path, 'w') as hf_:
    hf_.create_dataset('GENE x domain', data=booz.gene_x)
    hf_.create_dataset('GENE y domain', data=booz.gene_y)
    hf_.create_dataset('poloidal domain', data=booz.pol_dom)
    hf_.create_dataset('cartesian coordinates', data=booz.cart_points)
    hf_.create_dataset('q0', data=booz.q0)
    hf_.create_dataset('s0', data=booz.s0)
    hf_.create_dataset('alpha0', data=booz.alpha0)
    hf_.create_dataset('shat', data=booz.shat)
    hf_.create_dataset('Bref', data=booz.Bref)
    hf_.create_dataset('Lref', data=booz.Lref)
    hf_.create_dataset('rho ref', data=booz.rho_ref)
