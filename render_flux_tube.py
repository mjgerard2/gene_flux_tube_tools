import os
import h5py as hf
import numpy as np
import vtk_grids as vtkG

# Full path to flux tube data #
flux_tube_path = os.path.join(os.getcwd(), 'flux_tube_data.h5')

# Full path to where VTK file will be generated #
vtk_volume_path = os.path.join(os.getcwd(), 'flux_tube_volume.vtk')

# Read in electrostatic fluctuation data #
phi_path = os.path.join(os.getcwd(), 'phidata.h5')
with hf.File(phi_path, 'r') as hf_:
    time_key = 'time {0:0.0f}'.format(len(hf_)-1)
    phi_slice = hf_[time_key][()]

# Generate single vtk files #
vtkG.flux_tube_volume(flux_tube_path, vtk_volume_path, pol_beg=-np.pi, pol_end=np.pi, scalar_data=phi_slice, volume_element='VTK_TETRA')
