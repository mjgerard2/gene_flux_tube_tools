This directory is used to generate data structures that allow GENE flux tube data to be rendered
in real space via Paraview. Below is a description of the files in this directory.

Python Scripts (Version 3.10.6)
-------------------------------
read_boozmn.py  
	This is a class constructor that generates an object for a Stellopt xbooz_xform output
	file. This script is imported by other scripts in the directory.
  
read_parameters.py  
	This is a class constructor that generates an object for a GENE parameters output file.
	This script is imported by other scripts in the directory.
  
vtk_grids.py  
	This script has a list of functions that can be used to generate various kinds of VTK files.
	The "flux_tube_volume" function is used in render_flux_tube.py to generate a flux tube VTK
	file.
  
generate_flux_tube_data.py  
	This scripts transforms GENE flux tube data into Cartesian coordinates. This is used to
	generate an hdf5 file that stores the Cartesian coordiantes for all GENE grid points.
  
interpolated_phidata.py  
	This script interpolates electrostatic fluctuation data on an arbirtrary number of x-y
	GENE planes inbetween each simulated x-y GENE plane.
  
render_flux_tube.py  
	This script generates a vtk file for a flux tube geometry that can then be read by Paraview.
  
transform_phidata_to_hdf5.py  
	This script transforms the GENE output phidata file from an ascii format with a list of
	electrostatic flutuation data into an hdf5 file that can then be used to generate vtk files
	for Paraview.
  
Data Files (NetCDF format)
--------------------------
wout_QHS_best.nc  
	Output wout file from VMEC code in Stellopt package.
  
boozmn_wout_QHS_best.nc  
	Output boozmn file from xbooz_xform code in Stellopt package.
