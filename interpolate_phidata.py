import os
import h5py as hf
import numpy as np

import scipy.interpolate as spi

# Specify the number of interpolated GENE #
# x-y planes between each simulated x-y plane #
Ninterp = 3

# Full path to GENE hdf5 phidata file #
# Full path to interpolated phidata file that will be generated #
phi_path = os.path.join(os.getcwd(), 'phidata.h5')
new_phi_path = os.path.join(os.getcwd(), 'phidata_interp{}.h5'.format(Ninterp))

with hf.File(phi_path, 'r') as hf_:
    # Use zeroth time slice to define the grid parameters #
    print('initializing with \"time 0\"')
    t_slice = hf_['time 0'][()]

    Nz = t_slice.shape[0] * (Ninterp + 1) - Ninterp
    Ny = t_slice.shape[1]
    Nx = t_slice.shape[2]

    z_dom = np.linspace(-1, 1, t_slice.shape[0])
    z_interp = np.linspace(-1, 1, Nz)

    # Interpolate GENE x-y planes along zeroth time slice #
    t_slice_new = np.empty((Nz, Ny, Nx))
    for i in range(Ny):
        for j in range(Nx):
            phi_model = spi.interp1d(z_dom, t_slice[:, i, j])
            t_slice_new[:, i, j] = phi_model(z_interp)

    # Save interpolated x-y planes along zeroth time slice #
    with hf.File(new_phi_path, 'a') as hf_new:
        hf_new.create_dataset('time 0', data=t_slice_new)

    # Loop through subsequent time slices #
    tpts = len(hf_)
    for t in range(1, tpts):
        print('({}|{})'.format(t+1, tpts))
        time_key = 'time {}'.format(t)
        t_slice = hf_[time_key][()]

        # Interpolate GENE x-y planes along t^{th} time slice #
        t_slice_new = np.empty((Nz, Ny, Nx))
        for i in range(Ny):
            for j in range(Nx):
                phi_model = spi.interp1d(z_dom, t_slice[:, i, j])
                t_slice_new[:, i, j] = phi_model(z_interp)

        # Save interpolated x-y planes along t^{th} time slice #
        with hf.File(new_phi_path, 'a') as hf_new:
            hf_new.create_dataset(time_key, data=t_slice_new)
